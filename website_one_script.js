var getUrl = window.location;
var array = [];

var flights = document.querySelectorAll(".flights-table__flight");
var model = {
    DepartureTime: ".start-time",
    ArrivalTime: ".end-time",
    DepartureAirport: ".cities__departure",
    ArrivalAirport: ".cities__destination",
    FlightCode: ".flight-number",
    Duration: ".duration",
    Price: ".flights-table-price__price"
};

for (let flt of flights) {
    var singleFlight = {};

    for (var md in model) {

        if (flt.querySelector(model[md]) !== null) {
            singleFlight[md] = flt.querySelector(model[md]).childNodes[0].textContent.trim();
        }
    }
    if (singleFlight !== undefined || singleFlight.length !== 0) {
        array.push(singleFlight);
    }
}

var httprequest = new XMLHttpRequest();
httprequest.open("POST", "http://api.capturedata.ie");
httprequest.setRequestHeader("Content-Type", "application/json; charset=utf-8");
httprequest.send(JSON.stringify(array));
