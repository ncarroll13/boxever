var getUrl = window.location;
var array = [];

if (getUrl.host == "www.ryanair.com") {
    var flights = document.querySelectorAll(".flights-table__flight");
    var model = {
        DepartureTime: ".start-time",
        ArrivalTime: ".end-time",
        DepartureAirport: ".cities__departure",
        ArrivalAirport: ".cities__destination",
        FlightCode: ".flight-number",
        Duration: ".duration",
        Price: ".flights-table-price__price"
    };
} else if (getUrl.host == "www.flybe.com") {
    var flights = document.querySelectorAll(".flight-details");
    var model = {
        DepartureTime: ".depart-time .time",
        ArrivalTime: ".arrive-time .time",
        DepartureAirport: ".depart-time .airport-code",
        ArrivalAirpot: ".arrive-time .airport-code",
        FlightCode: ".flight-code",
        Duration: ".flight-length .length",
        Price: ".price .amount .formatted-currency"
    };
}

for (let flt of flights) {
    var singleFlight = {};

    for (var md in model) {

        if (flt.querySelector(model[md]) !== null) {
            singleFlight[md] = flt.querySelector(model[md]).childNodes[0].textContent.trim();
        }
    }
    if (singleFlight !== undefined || singleFlight.length !== 0) {
        array.push(singleFlight);
    }
}

var httprequest = new XMLHttpRequest();
httprequest.open("POST", "http://api.capturedata.ie");
httprequest.setRequestHeader("Content-Type", "application/json; charset=utf-8");
httprequest.send(JSON.stringify(array));
